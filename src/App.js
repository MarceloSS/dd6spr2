import './App.css';
import Home from './pages/Home';
import Button from './styledComp/button';
import ContentLine from './styledComp/contentLine';
import Title from './styledComp/title'

function App() {
  return <Home>
    <Title>Welcome!</Title>
    <input></input>
    <ContentLine>
      <Button>Search</Button>
      <Button>Clear</Button>
    </ContentLine>
  </Home>
}

export default App;
