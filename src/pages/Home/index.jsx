import { Container } from "./styles.js";

const Home = (props) => {
    return <Container>{props.children}</Container>
}

export default Home