import styled from "styled-components";

export const Container = styled.div`
    color: white;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    width: 100vw;
    height: 100vh;
    background-color: #682860;

    input{
        width: 50vw;
        background-color: #636BA6;
        padding: 5px;
        border-radius: 5px;
        border: 5px ridge #5DADEC;
        margin: 40px;
        color: white;
    }
    
    Button{
        width: 100px;
    }
    
    ContentLine{
        display: flex;
        flex-direction: row;
    }
`;