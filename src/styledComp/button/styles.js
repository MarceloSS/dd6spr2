import styled from "styled-components";

export const Container = styled.button`
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #636BA6;
    padding: 5px;
    border-radius: 5px;
    border: 5px ridge #5DADEC;
    margin: 10px;
    
    :hover{
        background-color: #664A83;
        border: 5px groove #5DADEC;
    }
    
`;