import { Container } from "./styles.js";

const ContentLine = (props) => {
    return <Container>{props.children}</Container>
}

export default ContentLine;