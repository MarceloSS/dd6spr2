import { Container } from './styles'

const Title = (props) => {
    return <Container><strong>{props.children}</strong></Container>
}
export default Title;