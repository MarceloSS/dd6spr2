import styled from "styled-components";

export const Container = styled.div`
    strong{
        display: inline-block;
        font-size: 100px;
        font-family: sans;
        color: #636BA6;
        -webkit-text-stroke: 2px #5DADEC;
        text-shadow: 0px 5px 20px black; 
        transform: scale(1, 1.5);
    }
`;